package com.example.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.example.rest.api.ApiErrorCodes;
import com.example.rest.api.RestApiError;
import com.example.rest.api.RestApiHttpStatus;
import com.example.util.JacksonUtils;

public class ApiAwareAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler
{
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException
	{
		String apiLoginHeader = request.getHeader(ApiAwareAuthenticationSuccessHandler.X_SPA_API_LOGIN);
		if ("true".equals(apiLoginHeader))
		{
			RestApiError restApiError = new RestApiError(RestApiHttpStatus.UNAUTHORIZED);
			restApiError.setApiCode(ApiErrorCodes.API_LOGIN_FAILURE);
			restApiError.setDeveloperMessage("Bad username / password combination");
			restApiError.setUserMessage("Invalid username / password combination");

			String json = JacksonUtils.toJSON(restApiError);

			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.getWriter().println(json);
		} else
		{
			super.onAuthenticationFailure(request, response, exception);
		}
	}
}
