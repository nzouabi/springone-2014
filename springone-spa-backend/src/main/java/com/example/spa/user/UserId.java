package com.example.spa.user;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;


final public class UserId implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final Integer pkey;

	public UserId(Integer pkey)
	{
		checkNotNull(pkey);
		this.pkey = pkey;
	}

	@Override
	public int hashCode()
	{
		return getPkey().hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof UserId)
		{
			return getPkey().equals(((UserId) obj).pkey);
		}
		return false;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("UserHandle [pkey=");
		builder.append(pkey);
		builder.append("]");
		return builder.toString();
	}

	public Integer getPkey()
	{
		return pkey;
	}
}