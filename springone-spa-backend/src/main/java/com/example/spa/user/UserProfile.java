package com.example.spa.user;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.collect.ImmutableList;

public final class UserProfile
{
	private final String name;
	private final UserId userId;
	private final ImmutableList<String> allEmails;

	public UserProfile(UserDetails userDetails, UserAccount userAccount)
	{
		this.name = userAccount.getName();
		this.userId = new UserId(userAccount.getPkey());

		List<String> emails = userAccount.getEmails();
		allEmails = ImmutableList.copyOf(emails);
	}

	@Override
	public String toString()
	{
		return "UserProfile [name=" + name + ", " + getUserId() + "]";
	}

	public String getName()
	{
		return name;
	}

	public List<String> getAllEmails() {
		return allEmails;
	}

	public UserId getUserId() {
		return userId;
	}
}
