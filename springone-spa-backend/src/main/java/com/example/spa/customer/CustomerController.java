package com.example.spa.customer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.api.exceptions.ResourceNotFoundRestApiException;


@RestController
public class CustomerController {
	
	private AtomicInteger counter = new AtomicInteger(0);
	private Map<Integer,CustomerJson> customers = new HashMap<>();

	private void add(String name, String email) {
		CustomerJson customerJson = new CustomerJson();		
		customerJson.setId(counter.addAndGet(1));
		customerJson.setName(name);
		customerJson.setEmail(email);		
		
		customers.put(customerJson.getId(), customerJson);
	}
	
	public CustomerController() {
		
		add("Jim Smith", "jim@example.com");
		add("James Joyce", "james@example.com");
			
	}
	
	@RequestMapping(value = ApiUrls.CUSTOMERS, method = RequestMethod.POST)	
	public CustomerJson post(@RequestBody @Valid CustomerJson customerJson)
	{			
		customerJson.setId(counter.getAndAdd(1));
		this.customers.put(customerJson.getId(), customerJson);
	    return customerJson;
	}
	
	@RequestMapping(ApiUrls.CUSTOMER)
	public CustomerJson get(@PathVariable("id") Integer id)
	{
		CustomerJson customerJson = this.customers.get(id);
		if(customerJson == null) {
			
			throw new ResourceNotFoundRestApiException()
					  .userMessage("User id %s does not exist",id)
					  .developerMessage("wrong user id the url");
		}
	    return customerJson;
	}

	@RequestMapping(value = ApiUrls.CUSTOMER, method = RequestMethod.PUT)	
	public CustomerJson put(@RequestBody CustomerJson customerJson)
	{			
		CustomerJson current = this.get(customerJson.getId());
		current.setName(customerJson.getName());
		current.setEmail(customerJson.getEmail());
		
		return customerJson; 
	}

	@RequestMapping(value = ApiUrls.CUSTOMER, method = RequestMethod.DELETE)	
	public void delete(@PathVariable("id") Integer id)
	{			
		this.customers.remove(id);
	}
}
