package com.example.rest.api.exceptions;

import com.example.rest.api.RestApiException;
import com.example.rest.api.RestApiHttpStatus;

public class InternalServerErrorRestApiException extends RestApiException
{
	private static final long serialVersionUID = 1L;

	public InternalServerErrorRestApiException()
	{
		super(RestApiHttpStatus.INTERNAL_SERVER_ERROR);
	}
}
